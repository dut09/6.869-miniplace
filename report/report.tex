\documentclass[10pt,twocolumn,letterpaper]{article}

\usepackage{cvpr}
\usepackage{times}
\usepackage{epsfig}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}

% Include other packages here, before hyperref.

% If you comment hyperref and then uncomment it, you should delete
% egpaper.aux before re-running latex.  (Or just hit 'q' on the first latex
% run, let it finish, and you should be clear).
\usepackage[breaklinks=true,bookmarks=false]{hyperref}

\cvprfinalcopy % *** Uncomment this line for the final submission

\def\cvprPaperID{****} % *** Enter the CVPR Paper ID here
\def\httilde{\mbox{\tt\raisebox{-.5ex}{\symbol{126}}}}

% Pages are numbered in submission mode, and unnumbered in camera-ready
%\ifcvprfinal\pagestyle{empty}\fi
\setcounter{page}{1}
\begin{document}

%%%%%%%%% TITLE
\title{Training Convolutional Neural Network for Mini Places Challenge}

\author{Tao Du\\
Massachusetts Institute of Technology\\
{\tt\small taodu@csail.mit.edu}
% For a paper whose authors are all at the same institution,
% omit the following lines up until the closing ``}''.
% Additional authors and addresses can be added with ``\and'',
% just like the second author.
% To save space, use either the email address or home page, not both
}

\maketitle
%\thispagestyle{empty}

%%%%%%%%% ABSTRACT
\begin{abstract}
   In this report we describe our way to train Convolutional Neural Network(CNN) for Mini Places Challenge. Our network achieves 0.295 top-5 error and 0.599 top-1 error in the test set. The main approaches we use are adding/removing neurons and data augmentation to avoid overfitting. We also propose a method to build a hybrid classifier based on our CNN, AlexNet and VGG. Finally the performances of different CNNs in the original and augmented training datasets are compared and analyzed.
\end{abstract}

%%%%%%%%% BODY TEXT
\section{Introduction}

In recent years, Convolutional Neural Network(CNN) has been widely used in many fields in artificial intelligence especially for visual recognition. With large-scale training datasets CNN has been shown to be a powerful tool for tasks like image and video classification, facial recognition and natural language processing.

The visual recognition task we are going to tackle in this report is scene recognition, which tries to answer the question ``which place is this image from?''. It is perhaps one of the most interesting and important problems in vision tasks in that it requires an overall understanding of the whole image, and it has a close relation between other tasks like object recognition.

Having seen the success of CNN in other visual tasks, it is natural to consider applying it into scene recognition. In this project we will use CNN to train a classifier for the scene recognition task. Specifically, our goal is to train a classifier to give top 5 predictions for a given image randomly chosen from 100 categories. This report walks through our training process as well as provides comparison and discussion regarding the performances of different approaches and CNNs.

This report is organized as follows: In Section 2 we review related work in deep learning, scene recognition and relevant dataset used in this project. Our approach to train the CNN is described in Section 3 with all the technical details. Section 4 provides training and testing results as well as in-depth analysis and comparison between different approaches we tried in this project. In Section 5 we conclude our method and discuss possible extensions for the future work.

%-------------------------------------------------------------------------
\section{Related work}

\subsection{Convolutional Neural Network} A typical CNN architecture consists of multiple layers. The lower layers extract local features, which are then forwarded into higher layers for detecting more nontrivial, nonintuitive features. As CNN contains numerous parameters, large-scale dataset is usually required to achieve good training results. In recent years CNN has been widely used in visual recognition tasks including object classification and detection~\cite{kavukcuoglu2010learning}, image classification~\cite{krizhevsky2012imagenet}, video classification~\cite{karpathy2014large}, viewpoint estimation~\cite{su2015render} and many more.

Due to the standard definition of neurons and layers, large volume of input data and heavy computation required in the training process, many CNN libraries have been developed to relieve people from tedious implementation details. Some of the most popular ones include Caffe~\cite{jia2014caffe}, Torch~\cite{torch}, MatConvNet~\cite{vedaldi2014matconvnet} and cuda-convnet~\cite{cuda-convnet}.


\subsection{Scene recognition datasets} Many datasets have been built for scene recognition. Early databases like Scene15~\cite{lazebnik2006beyond}, MIT Indoor67~\cite{quattoni2009recognizing} and SUN~\cite{xiao2010sun} contain scene images from 15 to 397 categories. ImageNet~\cite{imagenet_cvpr09} also provides 128 scene categories although it is originally developed for object recognition.

Although scene datasets are not new, most of them are relatively small for training CNN. Recently, a large scene-centric dataset Places~\cite{zhou2014learning} is provided for learning deep features for scene recognition, which contains over 7 million images from 476 categories. An even larger Places2~\cite{places2} dataset was released lately, covering over 10 million images from more than 400 scene categories. As far as we know this is the largest image dataset designed for scene recognition task.

In this report we will work on a small subset of the Places2 database. The dataset Mini Places contains 100,000 training images, 10,000 validation images and 10,000 test images from 100 categories.

%-------------------------------------------------------------------------
\section{CNN training}

In this section we provide all the technical details about our approach to train the CNN. The sections starts with a description of the network structure used in the challenge, followed by an introduction to our data augmentation approach. Finally we proposed a simple way to build a classifier synthesizing results from multiple CNNs.

\subsection{Network structure} In Mini Places Challenge we try the following 5 network structures: \textbf{reference network}: a CNN provided by the TA, including 3 convolutional layers, 2 fully connected layers and 1 dropout layer; \textbf{reduced reference network}: the same structure with the reference network but the number of neurons in each convolutional layer is reduced by a half; \textbf{Deeper reference network}: Add two more convolutional layers in the middle of the reference network; \textbf{AlexNet} and \textbf{VGG}: commonly used CNN structures with deeper layers and more neurons than the reference network. For AlexNet and VGG, the size of neurons in some layers and the length of the final output are modified to fit in Mini Places Challenge.

We ask readers to refer to our source code~\cite{dut09} for a complete definition of all the networks used in Mini Places Challenge.

\subsection{Data augmentation} Data augmentation is a widely used technique in training CNN. Common methods to augment image dataset include cropping, resizing and flipping. Although it is a convenient approach to add variation to the training data automatically, key features might be lost if an image is deformed dramatically, making it harder to tell its categories. To balance these effects we apply the following method for data augmentation, which we find gives us reasonable results.

For each 128$\times$128 image, we first select four random pixels from 40$\times$40 squares residing at its four corners. These four random points are used to crop the original image. The cropped image is further flipped in left/right direction to add variation. Finally the cropped image is rescaled to the original size so that they can be fed into the network seamlessly.

\subsection{Hybrid classifier} Another approach to boost the classification accuracy is to synthesize results from multiple classifiers which have comparable performances. For each test image, a classifier produces a 100-length vector showing the predictive score for each category. Given multiple classifiers we concatenate their outputs into a longer score vector and pick up the top 5 predictions. Roughly speaking, the idea behind this hybrid classifier is that we expect every CNN can outperform others in some categories, i.e., it gives strong positive classification results and dominates other CNNs, and therefore can be picked up by our hybrid classifier when images from that category are encountered. Note that more advanced methods can also be applied in this step, for example, one can collect the learning features from many CNNs then use SVM to train a new classifier, but we leave it in the future work.

\subsection{Choosing hyperparameters} Hyperparameters like learning rate, batch size, normalization options play a nontrivial role in the training process. They are typically set up in advance before the CNN training starts. For this challenge we use similar hyperparameters from ImageNet CNN training process provided in MatConvNet~\cite{vedaldi2014matconvnet}, but enable GPU options and adjust image loading process and image statistics calculation to fully exploit the computation of the server. Although we don't make an effort to tune hyperparameters, it turns out the current settings work reasonably well.

%-------------------------------------------------------------------------
\section{Experiments}

In this part we try different approaches described in the last section to train multiple CNNs and report their accuracy in the training, validation and test images. The error rate from the reference network is analyzed first and serves as a baseline for the follow up CNNs. We focus on comparison between networks enabling/disabling dropout layers, with/without data augmentation and with less/more neurons. Finally, the performance of the hybrid classifier is discussed in the end of this section.

All experiments are running on Amazon EC2 g2.8xlarge instance type with 4 GPUs fully enabled. Depending on the different size of the training dataset and the number of parameters in CNNs, the training time for one epoch varies from 2 minutes to 6 minutes. Typically a complete training process contains 60 to 90 epochs, taking from 2 hours to 9 hours in total.

The complete source code for initializing networks, augmenting dataset and building hybrid classifier can be accessed from~\cite{dut09} for people to reproduce the results in this section.

\subsection{Reference network} The reference net contains 16 layers trained for 60 epoch with
training and validation error listed in Table~\ref{table:refnet}(These data are given by the TA).

\begin{table}
	\begin{center}
		\begin{tabular}{|l|c|c|}
			\hline
			Error & Training & Validation \\
			\hline\hline
			Best top-1 error & 0.5199 & 0.6405 \\
			Best top-5 error & 0.2072 & 0.3427 \\
			Final top-1 error & 0.5206 & 0.6449 \\
			Final top-5 error & 0.2072 & 0.3511 \\
			\hline
		\end{tabular}
	\end{center}
	\caption{Training and validation error from the reference network.}
	\label{table:refnet}
\end{table}

To verify its performance, we rerun the reference network in the validation dataset, with the dropout layer removed because it is designed for avoiding overfitting during the training process only. The top-1 and top-5 errors we get are 0.6385 and 0.3397, which is consistent with the given data.

From the training and validation errors we conclude the reference network suffers from overfitting in the training dataset because the top-5 error is about 15\% lower compared with that in the validation set. Since a dropout layer has already been added into the reference network, this is a good indication that the reference network contains too many parameters, or the training data does not have rich enough images to feed into the network.

\subsection{Effects of a dropout layer} In CNN, a dropout layer randomly throws out units from the network during the training process. In order to have a deeper understanding of the overfit effect, before we simplify our network or augment the training data set, we are interested in how much benefit we can get from a dropout layer. Specifically, we temporarily remove the dropout layer from the reference net and see how much error we get in the training and validation dataset, shown in Table~\ref{table:dropout}.

\begin{table}
	\begin{center}
		\begin{tabular}{|l|c|c|}
			\hline
			Error & Training & Validation \\
			\hline\hline
			Best top-1 error & 0.3041 & 0.7491 \\
			Best top-5 error & 0.0876 & 0.4821 \\
			Final top-1 error & 0.3041 & 0.7509 \\
			Final top-5 error & 0.0876 & 0.4833 \\
			\hline
		\end{tabular}
	\end{center}
	\caption{Training and validation error from the reference network, with dropout layer removed.}
	\label{table:dropout}
\end{table}

\begin{figure}[t]
	\begin{center}
		\includegraphics[width=0.8\linewidth]{dropout.jpg}
	\end{center}
	\caption{Training and validation errors from reference network without dropout layer.}
	\label{fig:dropout}
\end{figure}

It can be seen from Figure~\ref{fig:dropout} and Table~\ref{table:dropout} that a reference network without a dropout layer gets trapped in severely overfitting the training images. This positive example demonstrates the importance of keeping the dropout layer in the network.

\subsection{Data augmentation} The original Mini Places Challenge training dataset contains 100,000 image covering 100 categories, with 1,000 image per category. We augment it by cropping, rescaling and flipping existing training images in each category. We generate two augmented datasets: training-2x and training-4x, which contains 2,000 and 4,000 images per
category respectively. A few examples generated by our data augmentation algorithm are displayed in Figure~\ref{fig:augment}.

\begin{figure}[t]
	\begin{center}
		\includegraphics[width=0.8\linewidth]{augment.jpg}
	\end{center}
	\caption{Examples of augmented training images from 6 categories. Each pair contains an original
		 image(left) and a deformed image(right). Upper left: abbey. Lower left: aquarium.
		 Upper middle: light house. Lower middle: temple. Upper right: volcano. Lower right: yard.}
	\label{fig:augment}
\end{figure}

To see the effectiveness of our augmented dataset we retrain the reference network in train-2x. The training and validation errors are given in Table~\ref{table:refnet-train-2x} and Figure~\ref{fig:refnet-train-2x}.

\begin{table}
	\begin{center}
		\begin{tabular}{|l|c|c|}
			\hline
			Error & Training & Validation \\
			\hline\hline
			Best top-1 error & 0.6143 & 0.6182 \\
			Best top-5 error & 0.3022 & 0.3160 \\
			Final top-1 error & 0.6143 & 0.6190 \\
			Final top-5 error & 0.3039 & 0.3199 \\
			\hline
		\end{tabular}
	\end{center}
	\caption{Training and validation errors from the reference network in augmented
		 training dataset train-2x.}
	\label{table:refnet-train-2x}
\end{table}

\begin{figure}[t]
	\begin{center}
		\includegraphics[width=0.8\linewidth]{refnet-train-2x.jpg}
	\end{center}
	\caption{Training and validation errors from reference network with augmented
		 dataset train-2x.}
	\label{fig:refnet-train-2x}
\end{figure}

By comparing Table~\ref{table:refnet-train-2x} with Table~\ref{table:refnet} we conclude the augmented dataset improves the accuracy up to 4\%. This is as expected because previously the reference network has an issue with overfitting. Moreover, we note that the top-1 and top-5 errors in both training and validation sets are close to each other, which is also a good sign that the network can be well generalized to the test set.

\subsection{Removing neurons} Another way to avoid overfitting is to simplify the network structure by using smaller and less filters in each layer. As described in the last section, we aggressively remove half neurons in each convolutional layer, making the size of the reduced network much smaller than the original reference network. To improve the performance
further we apply the reduced network in train-4x. The errors and plots are displayed in Table~\ref{table:red-train-4x} and Figure~\ref{fig:red-train-4x}.

\begin{table}
	\begin{center}
		\begin{tabular}{|l|c|c|}
			\hline
			Error & Training & Validation \\
			\hline\hline
			Best top-1 error & 0.6290 & 0.6119 \\
			Best top-5 error & 0.3211 & 0.3082 \\
			Final top-1 error & 0.6293 & 0.6123 \\
			Final top-5 error & 0.3213 & 0.3101 \\
			\hline
		\end{tabular}
	\end{center}
	\caption{Training and validation errors from reduced network in train-4x.}
	\label{table:red-train-4x}
\end{table}

\begin{figure}[t]
	\begin{center}
		\includegraphics[width=0.8\linewidth]{refnet-train-4x.jpg}
	\end{center}
	\caption{Training and validation errors from reduced network in train-4x.}
	\label{fig:red-train-4x}
\end{figure}

Although the dataset is enlarged further and parameters are dropped, the improvement is negligible(up to less than 1\% in top-5 error in the validation set). As a result, the reduced reference network might be over simplified to handle the augmented dataset, which inspires us to train the deeper reference network.

\subsection{Adding more neurons} As mentioned in the last section our deeper reference network shares similar structure with the original and reduced one but adds two convolutional layers. The errors are displayed in Table~\ref{table:deeper-train-4x} and Figure~\ref{fig:deeper-train-4x}. Note that this deeper reference network achieves the best classification results and is used in Mini Places Challenge, with top-1 error = 0.599 and top-5 error = 0.295.

\begin{table}
	\begin{center}
		\begin{tabular}{|l|c|c|c|}
			\hline
			Error & Training & Validation & Test \\
			\hline\hline
			Best top-1 error & 0.5748 & 0.5817 & - \\
			Best top-5 error & 0.2663 & 0.2827 & - \\
			Final top-1 error & 0.5748 & 0.5838 & 0.599 \\
			Final top-5 error & 0.2677 & 0.2827 & 0.295 \\
			\hline
		\end{tabular}
	\end{center}
	\caption{Training and validation errors from deeper reference network in train-4x.}
	\label{table:deeper-train-4x}
\end{table}

\begin{figure}[t]
	\begin{center}
		\includegraphics[width=0.8\linewidth]{deeper-train-4x.jpg}
	\end{center}
	\caption{Training and validation errors from deeper reference network in train-4x.}
	\label{fig:deeper-train-4x}
\end{figure}

From these data we conclude that with deeper layer networks and larger training dataset it is possible to push the limits a little more(up to 3\% compared with the reduced reference network). Moreover, it is likely to see even better results by extending the dataset more greatly, or applying deeper networks, but due to the time and hardware constraints we don't implement it and instead leave it as future work.

\subsection{Other CNN structures} It is also worthwhile to try existing CNN structures and see how they perform in the Mini Places Challenge. In this section we provide results from training AlexNet and VGG in our augmented dataset train-2x. Tiny modifications are made on the filter size so that our 128$\times$128 images can fit into the networks.

Table~\ref{table:alexnet} and Figure~\ref{fig:alexnet} show the errors of AlexNet. The training process is planned to take 60 epochs but is terminated early at epoch 37 as we don't see much chance that it can outperform the deeper reference network. The results for VGG is shown in Table~\ref{table:vgg} and Figure~\ref{fig:vgg} and the training process is terminated early for the same reason.

\begin{table}
	\begin{center}
		\begin{tabular}{|l|c|c|}
			\hline
			Error & Training & Validation \\
			\hline\hline
			Best top-1 error & 0.6942 & 0.6827 \\
			Best top-5 error & 0.3905 & 0.3812 \\
			Final top-1 error & 0.6942 & 0.6828 \\
			Final top-5 error & 0.3905 & 0.3814 \\
			\hline
		\end{tabular}
	\end{center}
	\caption{Training and validation errors from AlexNet in train-2x.}
	\label{table:alexnet}
\end{table}

\begin{figure}[t]
	\begin{center}
		\includegraphics[width=0.8\linewidth]{alexnet.jpg}
	\end{center}
	\caption{Training and validation errors from AlexNet in train-2x. The training process
		is terminated at epoch 37.}
	\label{fig:alexnet}
\end{figure}

\begin{table}
	\begin{center}
		\begin{tabular}{|l|c|c|}
			\hline
			Error & Training & Validation \\
			\hline\hline
			Best top-1 error & 0.6832 & 0.6736 \\
			Best top-5 error & 0.3787 & 0.3750 \\
			Final top-1 error & 0.6832 & 0.6813 \\
			Final top-5 error & 0.3787 & 0.3750 \\
			\hline
		\end{tabular}
	\end{center}
	\caption{Training and validation errors from VGG in train-2x.}
	\label{table:vgg}
\end{table}

\begin{figure}[t]
	\begin{center}
		\includegraphics[width=0.8\linewidth]{vgg.jpg}
	\end{center}
	\caption{Training and validation errors from VGG in train-2x. The training process
		is terminated at epoch 36.}
	\label{fig:vgg}
\end{figure}

Although both AlexNet and VGG use much more parameters, making them significantly more
complex than our deeper reference network, the error rates up to epoch 36 don't convince us
that they can make obvious improvements. One possible reason is that both networks have not
been carefully fine tuned for scene recognition tasks specifically in this project, which we leave as future work.

\subsection{Hybrid classifier} We finally test our hybrid classifier built upon the following three networks: deeper reference network, AlexNet and VGG. It might be tempting to not include AlexNet and VGG and build the hybrid classifier by synthesizing reduced and deeper network, as they give best results by far. However, since both reduced and deeper reference networks have more or less the same structures, it is likely that they share very similar prediction errors. As a result we choose to include AlexNet and VGG in our hybrid classifier. The results are shown in Table~\ref{table:hybrid}. Note that the hybrid classifier is only evaluated in the validation set.

\begin{table}
	\begin{center}
		\begin{tabular}{|l|c|}
			\hline
			Error & Validation \\
			\hline\hline
			Final top-1 error & 0.6078 \\
			Final top-5 error & 0.3122 \\
			\hline
		\end{tabular}
	\end{center}
	\caption{Training and validation errors from hybrid classifier.}
	\label{table:hybrid}
\end{table}

It is interesting to notice that the final accuracy lies between the accuracies from the three networks, although one might expect lower error rate can be achieved because more information is provided by AlexNet and VGG. To get a better understanding of it we analyze the top 10 categories which produce most errors and visualize errors from all three networks, which is displayed in Figure~\ref{fig:error}.

\begin{figure*}
	\begin{center}
		\includegraphics[width=0.8\linewidth]{error.jpg}
	\end{center}
	\caption{Errors for deeper reference network, AlexNet, VGG and hybrid classifier among categories which produce 10 most errors.}
	\label{fig:error}
\end{figure*}

From the visualization we can conclude that the deeper reference network outperforms the other two in almost every category. So it turns out the classification results from AlexNet and VGG actually corrupts the hybrid classifier. This explains why including AlexNet and VGG helps little in improving the accuracy.

%-------------------------------------------------------------------------
\section{Conclusion}

In this report we introduce our effort to train a CNN for Mini Places Challenges. The reference network works as a good starting point for Mini Places Challenge but it suffers from overfitting in the original training dataset. To tackle this two approaches are applied: augmenting the image set, and adjusting the network layers and neurons. Larger training dataset improves the accuracy, but the network layers or the number of neurons needs to be adjusted accordingly to overcome potential underfit. The resulting deeper reference network achieves 0.295 top-5 error, which is better than the reference network, AlexNet and VGG(not well tuned though). The hybrid classifier does not improve this result further possibly due to that the deeper network dominates other CNNs in most of the categories.

The classification accuracy can be further increased by considering nontrivial hybrid classifiers, or by extending dataset and adding more layers, or by fine tuning widely used CNNs like AlexNet or VGG.

{\small
\bibliographystyle{ieee}
\bibliography{egbib}
}

\end{document}
