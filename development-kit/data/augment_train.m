% Tao Du
% taodu@csail.mit.edu
% Dec 1, 2015
%
% Augment train.txt.

fid = fopen('categories.txt', 'r');
train_info = textscan(fid, '%s %d');
fclose(fid);

% Now open up a new file.
fid = fopen('train.augmented.txt', 'w');
n = numel(train_info{1});
m = 2000; % Number of images per category.
d = 2000; % Number of images to add per category.
for i = 1 : n
    for j = 1 : m + d
        name = sprintf('train%s/%08d.jpg', train_info{1}{i}, j);
        fprintf(fid, '%s %d\n', name, train_info{2}(i));
    end
end
fclose(fid);

%% Augment images.
data_folder = fullfile('..', '..', 'data', 'images');
image_size = 128;
border_range = 40;
for i = 1 : n
    fprintf('.');
    if mod(i, 20) == 0
        fprintf('\n');
    end
    for j = 1 : d
        name = sprintf('train%s/%08d.jpg', train_info{1}{i}, j);
        name_aug = sprintf('train%s/%08d.jpg', train_info{1}{i}, j + m);
        image = imread(fullfile(data_folder, name));
        
        % Crop the image.
        % Generate randon numbers in [1 : border_range]^2
        % x [1 : border_range + image_size - border_range]^2.
        corner = randi([1, border_range], 1, 4);
        image_aug = image(corner(1) : corner(2) + image_size - border_range, ...
            corner(3) : corner(4) + image_size - border_range, :);
        
        % Resize the image.
        image_aug = imresize(image_aug, [image_size, image_size]);
        
        % Flip the image.
        image_aug = fliplr(image_aug);
        
        % Write back the image.
        imwrite(image_aug, fullfile(data_folder, name_aug));
    end
end
