% Tao Du
% taodu@csail.mit.edu
% Dec 7, 2015
%
% Generate test.txt with faked categories.

fid = fopen('test.txt', 'w');
images = dir(fullfile('..', '..', 'data', 'images', 'test', '*.jpg'));

n = numel(images);
for i = 1 : n
    fprintf(fid, 'test/%s %d\n', images(i).name, 0);
end

fclose(fid);