% Tao Du
% taodu@csail.mit.edu
% Nov 23, 2015
%
% Run the reference network on the validation/test set.
clear all; clc; close all;

% Load model.
gitCommitId = {'1d6177', '41d37a', '0bfe9e'};
epochNum = {'90', '37', '36'};
networkNum = numel(epochNum);
nets = cell(networkNum, 1);
for i = 1 : networkNum
    load(['data/miniplace-', gitCommitId{i}, '/net-epoch-', epochNum{i}, '.mat']);
    nets{i} = net;
end
% Load categories.
load('categoryIDX.mat');

% The path to the val/test set.
datatype = 'val'; % Change to 'test' if necessary.
dataset_path = '../data/images/';

% The output file path.
output_path = ['../development-kit/evaluation/miniplace.', datatype, ...
    '.txt'];
fid = fopen(output_path, 'w');

% Loop over all the images.
image_path = fullfile('..', 'development-kit', 'data', [datatype, '.txt']);
image_file = fopen(image_path);
image_info = textscan(image_file, '%s %d');
fclose(image_file);
image_names = image_info{1};
image_labels = image_info{2};
n = numel(image_names);

% Error for each category.
cat_error = zeros(1, 100);

for j = 1 : n
    % Display the progress. Print a dot every percent.
    if mod(j, uint16(n / 100)) == 0
        fprintf('.');
    end
    % Change line for every 20 percent.
    if mod(j, 20 * uint16(n / 100)) == 0
        fprintf('\n');
    end
    % Load and preprocess an image.
    im_name = image_names{j};
    im = imread([dataset_path, im_name]);
    im_resize = imresize(im, net.normalization.imageSize(1 : 2));
    im_ = single(im_resize); 
    for i = 1 : 3
        im_(:, :, i) = im_(:, :, i) - net.normalization.averageImage(i);
    end

    % Run all the CNN.
    scores = [];
    idx = [];
    for k = 1 : networkNum
        net = nets{k};
        % Change the last layer of CNN from softmaxloss to softmax.
        net.layers{1, end}.type = 'softmax';
        net.layers{1, end}.name = 'prob';
        % Remove all the dropout layers.
        n_layers = numel(net.layers);
        nodropout = [];
        for t = 1 : n_layers
            if isempty(strfind(net.layers{t}.name, 'dropout'))
                nodropout(end + 1) = t;
            end
        end
        net.layers = net.layers(nodropout);
        res = vl_simplenn(net, im_);
        scores = [scores; squeeze(gather(res(end).x))];
        idx = [idx 1 : 100];
    end

    [score_sort, idx_sort] = sort(scores, 'descend');
    idx = unique(idx(idx_sort), 'stable');

    % Remove duplicated categories.
    
    fprintf(fid, '%s %d %d %d %d %d\n', im_name, ...
        categoryIDX{idx(1), 2}, ...
        categoryIDX{idx(2), 2}, ...
        categoryIDX{idx(3), 2}, ...
        categoryIDX{idx(4), 2}, ...
        categoryIDX{idx(5), 2});

    label = image_labels(j) + 1;
    if ~ismember(label, idx(1 : 5))
        cat_error(label) = cat_error(label) + 1;
    end
end
fclose(fid);

% Plot error for all categories.
plot(1 : 100, cat_error);

%% Analyze and plot errors.
% Assume data have been loaded in memory.
[~, error_index] = sort(hybrid, 'descend');
top10_error_index = error_index(1 : 10);

hybrid_10 = hybrid(top10_error_index);
deeper_10 = deeper(top10_error_index);
alex_10 = alex(top10_error_index);
vgg_10 = vgg(top10_error_index);

name = cell(1, 10);
for i = 1 : 10
    category_name = categoryIDX(top10_error_index(i));
    category_name = category_name{1};
    % Remove everything before /.
    slashes = strfind(category_name, '/');
    category_name = category_name(slashes(end) + 1 : end);
    % If it contains underscore replace it with \_
    category_name = strrep(category_name, '_', '\_');
    name{i} = category_name;
end
bar([deeper_10; alex_10; vgg_10; hybrid_10]');
legend('deeper', 'alex', 'vgg', 'hybrid');
set(gca,'xticklabel',name)
xlabel('category name');
ylabel('error(%)');