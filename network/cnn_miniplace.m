function [net, info] = cnn_miniplace(varargin)
% Tao Du
% taodu@csail.mit.edu
% Dec 1, 2015
%
% CNN training.
run(fullfile(fileparts(mfilename('fullpath')),...
  '..', 'matconvnet', 'matlab', 'vl_setupnn.m'));

opts.dataDir = fullfile('..', 'data', 'images');
opts.modelType = 'miniplace';
opts.networkType = 'simplenn';
opts.batchNormalization = false;
opts.weightInitMethod = 'gaussian';
[opts, varargin] = vl_argparse(opts, varargin) ;

% Warning: this could be dangerous. Remember to NOT override existing data.
opts.expDir = fullfile('data', 'miniplace');
[opts, varargin] = vl_argparse(opts, varargin);

opts.numFetchThreads = 480;
opts.imdbPath = fullfile(opts.expDir, 'imdb.mat');
opts.train.batchSize = 2000;
opts.train.numSubBatches = 1;
opts.train.continue = true;
opts.train.gpus = [1, 2, 3, 4];
opts.train.prefetch = true;
opts.train.sync = false;
opts.train.cudnn = true;
opts.train.expDir = opts.expDir;
if ~opts.batchNormalization
  opts.train.learningRate = logspace(-2, -4, 90);
else
  opts.train.learningRate = logspace(-1, -4, 20);
end
[opts, varargin] = vl_argparse(opts, varargin);

opts.train.numEpochs = numel(opts.train.learningRate) ;
opts = vl_argparse(opts, varargin) ;

% --------------------------------------------------------------------
%                                                         Prepare data
% --------------------------------------------------------------------
disp('prepare data ...');
if exist(opts.imdbPath, 'file')
  imdb = load(opts.imdbPath);
else
  imdb = cnn_miniplace_setup_data('dataDir', opts.dataDir);
  mkdir(opts.expDir);
  save(opts.imdbPath, '-struct', 'imdb');
end

% --------------------------------------------------------------------
%                                               Network initialization 
% --------------------------------------------------------------------
disp('init cnn ...');
net = cnn_miniplace_init('model', opts.modelType, ...
                        'batchNormalization', opts.batchNormalization, ...
                        'weightInitMethod', opts.weightInitMethod);
bopts = net.normalization;
bopts.numThreads = opts.numFetchThreads;

% compute image statistics (mean, RGB covariances etc)
disp('get image stats ...');
imageStatsPath = fullfile(opts.expDir, 'imageStats.mat');
if exist(imageStatsPath, 'file')
  load(imageStatsPath, 'averageImage', 'rgbMean', 'rgbCovariance');
else
  [averageImage, rgbMean, rgbCovariance] = getImageStats(imdb, bopts);
  save(imageStatsPath, 'averageImage', 'rgbMean', 'rgbCovariance');
end

% One can use the average RGB value, or use a different average for
% each pixel
net.normalization.averageImage = averageImage;
%net.normalization.averageImage = rgbMean;

% --------------------------------------------------------------------
%                                          Stochastic gradient descent
% --------------------------------------------------------------------
[v, d] = eig(rgbCovariance);
bopts.transformation = 'stretch';
bopts.averageImage = rgbMean;
bopts.rgbVariance = 0.1 * sqrt(d) * v';

disp('training ...');
fn = getBatchSimpleNNWrapper(bopts);
[net, info] = cnn_train(net, imdb, fn, opts.train, 'conserveMemory', true);

% -------------------------------------------------------------------------
function fn = getBatchSimpleNNWrapper(opts)
% -------------------------------------------------------------------------
fn = @(imdb,batch) getBatchSimpleNN(imdb, batch, opts) ;

% -------------------------------------------------------------------------
function [im,labels] = getBatchSimpleNN(imdb, batch, opts)
% -------------------------------------------------------------------------
images = strcat([imdb.imageDir filesep], imdb.images.name(batch));
im = cnn_miniplace_get_batch(images, opts, ...
                            'prefetch', nargout == 0);
labels = imdb.images.label(batch);

% -------------------------------------------------------------------------
function [averageImage, rgbMean, rgbCovariance] = getImageStats(imdb, opts)
% -------------------------------------------------------------------------
train = find(imdb.images.set == 1);
bs = 1000;
fn = getBatchSimpleNNWrapper(opts);
for t = 1 : bs : numel(train)
  batch_time = tic;
  batch = train(t : min(t + bs - 1, numel(train)));
  fprintf('collecting image stats: batch starting with image %d ...', batch(1));
  temp = fn(imdb, batch);
  z = reshape(permute(temp, [3 1 2 4]), 3, []);
  n = size(z, 2);
  avg{t} = mean(temp, 4);
  rgbm1{t} = sum(z, 2) / n;
  rgbm2{t} = z * z' / n;
  batch_time = toc(batch_time);
  fprintf(' %.2f s (%.1f images/s)\n', batch_time, numel(batch)/ batch_time);
end
averageImage = mean(cat(4, avg{:}), 4);
rgbm1 = mean(cat(2, rgbm1{:}), 2);
rgbm2 = mean(cat(3, rgbm2{:}), 3);
rgbMean = rgbm1;
rgbCovariance = rgbm2 - rgbm1 * rgbm1';
disp('getImageStats done.');
