function imdb = cnn_miniplace_setup_data(varargin)
opts.dataDir = '';
[opts, ~] = vl_argparse(opts, varargin);

% -------------------------------------------------------------------------
%                                                  Load categories metadata
% -------------------------------------------------------------------------

catPath = fullfile('..', 'development-kit', 'data', 'categories.txt');
catFile = fopen(catPath);
catInfo = textscan(catFile, '%s %d');
fclose(catFile);
imdb.classes.name = catInfo{1};
imdb.imageDir = opts.dataDir;

% -------------------------------------------------------------------------
%                                                           Training images
% -------------------------------------------------------------------------

fprintf('loading training images ...\n');

trainPath = fullfile('..', 'development-kit', 'data', 'train.augmented.txt');
trainFile = fopen(trainPath);
trainInfo = textscan(trainFile, '%s %d');
fclose(trainFile);
names = trainInfo{1}';
labels = single(trainInfo{2}') + 1;
imdb.images.id = 1:numel(names);
imdb.images.name = names;
imdb.images.set = ones(1, numel(names));
imdb.images.label = labels;

% -------------------------------------------------------------------------
%                                                         Validation images
% -------------------------------------------------------------------------
valPath = fullfile('..', 'development-kit', 'data', 'val.txt');
valFile = fopen(valPath);
valInfo = textscan(valFile, '%s %d');
fclose(valFile);
names = valInfo{1}';
labels = single(valInfo{2}') + 1;

imdb.images.id = horzcat(imdb.images.id, (1 : numel(names)) + numel(imdb.images.id));
imdb.images.name = horzcat(imdb.images.name, names);
imdb.images.set = horzcat(imdb.images.set, 2 * ones(1,numel(names)));
imdb.images.label = horzcat(imdb.images.label, labels);
