% Modified by:
% Tao Du
% taodu@csail.mit.edu
% Nov 23, 2015
%
% Run the reference network and verify the accuracy on the validation set.

% install MatConvNet first at http://www.vlfeat.org/matconvnet/
clear all; clc; close all;

% load pre-trained model
load('categoryIDX.mat');
path_model = 'refNet1-epoch-60.mat';
load(path_model);

% The path to the validation set.
val_path = '../../data/images/val/';

% The output file path.
output_path = '../../development-kit/evaluation/refnet.val.txt';
fid = fopen(output_path, 'w');

% Loop over all the validation images.
image_names = dir([val_path, '*.jpg']);
n = numel(image_names);

% change the last layer of CNN from softmaxloss to softmax
net.layers{1, end}.type = 'softmax';
net.layers{1, end}.name = 'prob';

h = waitbar(0, 'working on the validation set...');
for j = 1 : n
    % load and preprocess an image
    im_name = image_names(j).name;
    im = imread([val_path, im_name]);
    im_resize = imresize(im, net.normalization.imageSize(1 : 2));
    im_ = single(im_resize); 
    for i = 1 : 3
        im_(:, :, i) = im_(:, :, i) - net.normalization.averageImage(i);
    end

    % run the CNN
    res = vl_simplenn(net, im_);

    scores = squeeze(gather(res(end).x));
    [score_sort, idx_sort] = sort(scores, 'descend');
    
    fprintf(fid, '%s %d %d %d %d %d\n', ['val/', im_name], ...
        categoryIDX{idx_sort(1), 2}, ...
        categoryIDX{idx_sort(2), 2}, ...
        categoryIDX{idx_sort(3), 2}, ...
        categoryIDX{idx_sort(4), 2}, ...
        categoryIDX{idx_sort(5), 2});
    waitbar(j / n);
end
close(h);
fclose(fid);